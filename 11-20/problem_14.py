"""
The following iterative sequence is defined for the set of positive integers:

n → n/2 (n is even)
n → 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:

13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms.
Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?

NOTE: Once the chain starts the terms are allowed to go above one million.
"""
from funcy import even

		
def get_len_collatz_sequence(num: int) -> int:
	collatz_sequence = 2  # we begin fill length with start number and 1
	while num != 1:
		if even(num):
			num //= 2
		else:
			num = 3 * num + 1
		collatz_sequence += 1
	return collatz_sequence


def get_answer(start: int, end: int=0) -> int:
	"""
	Get number with longest chain
	"""
	max_len_sequence = 0
	result = None
	for i in range(start, end, -1):
		current_length = get_len_collatz_sequence(i)
		if current_length > max_len_sequence:
			max_len_sequence = current_length
			result = i
	return result


if __name__ == "__main__":
	print(get_answer(start=999_999))  # 837_799
