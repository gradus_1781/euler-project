"""
Starting in the top left corner of a 2x2 grid, and only being able to move to the right and down,
there are exactly 6 routes to the bottom right corner.
https://projecteuler.net/problem=15
How many such routes are there through a 20x20 grid?
"""


def get_answer(row: int, column: int) -> int:
    """
    Get all possible routes through 20x20 grid
    """
    a = []
    for i in range(row + 2):
        a.append([0] * (column + 2))

    a[1][1] = 1

    for i in range(1, row + 2):
        for j in range(1, column + 2):
            if a[i][j] != 1:
                a[i][j] = a[i - 1][j] + a[i][j - 1]

    result = a[-1][-1]
    return result


if __name__ == "__main__":
    print(get_answer(row=20, column=20))  # 137_846_528_820
