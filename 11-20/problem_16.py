"""
2 ** 15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.

What is the sum of the digits of the number 2 ** 1000?
"""

def get_answer(degree: int) -> int:
	"""
	Get sum of the digits of the number 2 with given degree
	"""
	return sum([int(i) for i in str(2 ** degree)])


if __name__ == "__main__":
	print(get_answer(degree=1000))  # 1_366