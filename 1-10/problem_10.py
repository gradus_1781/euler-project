﻿"""
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million
"""


def get_answer(border_number: int) -> int:
    """
    Get sum of all primes below 2 million
    """
    current_list = list(range(border_number + 1))
    current_list[1] = 0
    current_index = 2
    intermediate_list = []
    # sieve of eratosthenes
    while current_index <= border_number:
        if current_list[current_index] != 0:
            intermediate_list.append(current_list[current_index])
            for j in range(current_index, border_number + 1, current_index):
                current_list[j] = 0
        current_index += 1
    answer = sum(intermediate_list)
    return answer


if __name__ == '__main__':
    print(get_answer(border_number=2_000_000))  # 142_913_828_922
