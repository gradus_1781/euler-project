﻿"""
A palindromic number reads the same both ways.
The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 x 99.
Find the largest palindrome made from the product of two 3-digit numbers.
"""
from itertools import combinations_with_replacement as c_w_r, starmap


def get_answer() -> int:
    """
    Return largest palindrome product
    """

    def is_palindrome(number):
        return str(number) == str(number)[::-1]

    nums = list(range(100, 999 + 1))
    nums1 = list(c_w_r(nums, 2))  # find all possible multiplying combinations between 2 numbers with 3-digit
    nums2 = list(starmap(lambda a, b: a * b, nums1))
    nums3 = [i for i in nums2 if is_palindrome(i)]
    result = max(nums3)
    return result


if __name__ == "__main__":
    print(get_answer())  # 906_609
