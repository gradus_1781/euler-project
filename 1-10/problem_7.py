﻿"""
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10 001st prime number?
"""


def get_answer(number: int):
    """
    Returns the prime number at the given position
    """
    def is_prime(num: int) -> bool:
        """
        Checks if a number is prime
        """
        for i in range(2, int(num ** 0.5) + 1):
            if num % i == 0:
                return False
        return True

    def prime_number(n: int) -> int:
        a = 1
        if n == 1:
            return 2
        else:
            for _ in range(1, n):
                a += 2
                while is_prime(a) == False:
                    a += 2
            return a

    return prime_number(number)  


if __name__ == "__main__":
    print(get_answer(10001))  # 104_743
