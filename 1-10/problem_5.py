﻿"""
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
"""
from math import gcd


def get_answer(input_data: int) -> int:
    """
    Get smallest multiple by all numbers from 1 to input_data
    """
    def gcm(a, b):
        return a * b // gcd(a, b)

        
    result = 1
    for divisor in range(2, input_data + 1): # enumeration of divisors
        result = gcm(result, divisor)  # finding greatest common multiple
    return result  


if __name__ == "__main__":
    print(get_answer(20)) # 232_792_560
