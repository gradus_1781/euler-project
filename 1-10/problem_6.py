﻿"""
The sum of the squares of the first ten natural numbers is,
12 + 22 + ... + 102 = 385

The square of the sum of the first ten natural numbers is,
(1 + 2 + ... + 10)2 = 552 = 3025

Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is .
3025 - 385 = 2640.

Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
"""


def get_answer(input_data: int) -> int:
    """
    Get difference of sum squared and sum of squares first of 100 natural numbers
    """
    sum_squared = sum_of_squares = 0
    for number in range(1, input_data + 1):
        sum_squared += number
        sum_of_squares += number ** 2
    result = sum_squared ** 2 - sum_of_squares
    return result


if __name__ == "__main__":
    print(get_answer(100))  # 25_164_150
