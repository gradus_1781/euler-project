﻿"""
The prime factors of 13195 are 5, 7, 13 and 29.
What is the largest prime factor of the number 600851475143 ?
"""


def get_answer(input_data: int) -> int:
    """
    Return largest prime factor
    """
    # get all divisors of input data
    n = set((sum([[i, input_data // i] for i in range(1, int(input_data ** 0.5) + 1) if input_data % i == 0], [])))
    print(n)

    def is_prime(num):
        for i in range(2, num):
            if num % i == 0:
                return False
        return True

    result = max([i for i in n if is_prime(i)])  # get maximum prime factor
    return result


if __name__ == "__main__":
    print(get_answer(600_851_475_143))  # 6_857
