﻿"""
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9.
The sum of these multiples is 23.
Find the sum of all the multiples of 3 or 5 below 1000.
"""


def get_answer(input_data: int) -> int:
    """
    Return sum of all numbers lower than 1000 and multiples to 3 or 5.
    """
    sum_of_numbers = 0
    for number in range(1, input_data):
        if number % 3 == 0 or number % 5 == 0:
            sum_of_numbers += number
    return sum_of_numbers


if __name__ == "__main__":
    print(get_answer(1000))  # 233_168
